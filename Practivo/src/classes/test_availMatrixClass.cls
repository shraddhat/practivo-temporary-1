@istest
public with sharing class test_availMatrixClass {
     static testmethod void testAllFunctional()
     {
         availableMatrix  availableWeekCalendar = new availableMatrix();
         
         test.startTest();
         availableWeekCalendar.timeSlotMatrix = Utils.initMatrix(availableWeekCalendar.timeSlotMatrix,700,1800,1);
         availableWeekCalendar.checkForAllSlotFull();
         system.assertEquals(true, availableWeekCalendar.isAllDayFull);
         test.stopTest();
     }
}