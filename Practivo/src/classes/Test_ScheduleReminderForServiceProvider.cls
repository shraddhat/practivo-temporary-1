@isTest
public class Test_ScheduleReminderForServiceProvider 
{
    public static String CRON_EXP = '0 0 06 * * ?';
static testmethod void testjob()
{
    Contact con=TestDataUtils.getServiceProvider();
    Location__c loc=TestDataUtils.getlocation();
    Category__c cat=TestDataUtils.getcategory();
    AppointmentType__c apt=TestDataUtils.getappointment(cat);
    apt.Maximum_Participant__c=5;
    update apt;
    Location_Category_Mapping__c locmap=TestDataUtils.getlocationmapping(loc, cat, con);
    Event ev=TestDataUtils.geteventappointmentwithall(con,locmap,apt);
    ev.EndDateTime=Date.Today().addDays(1);
    update ev;
      Test.startTest();

      // Schedule the test job
      String jobId = System.schedule('ScheduleApexClassTest1',
                        CRON_EXP, 
                        new ScheduleReminderForServiceProvider());
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
         NextFireTime
         FROM CronTrigger WHERE id = :jobId];
        System.assertNotEquals(null, ct);
        System.assertEquals(0, ct.TimesTriggered);
       test.stopTest();
}
}