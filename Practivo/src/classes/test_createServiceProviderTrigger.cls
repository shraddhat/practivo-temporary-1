@isTest
public class test_createServiceProviderTrigger {
    
    static testMethod void setUpData()
    {
        Profile prof = [SELECT Id FROM Profile WHERE Name =:Utils.serviceProviderProfile]; 
        system.debug('**prof**'+prof);
        User usr = new User(Alias = 'standt', Email='testuserepractice@Appointed.com', 
            EmailEncodingKey='UTF-8', LastName='ePracticeTesting', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = prof.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testuserepractice@appointed.com');
        insert usr;
        system.debug('*** USR ***'+usr); 
        system.runAs(usr) {
          
             Contact Con = [SELECT Id,LastName from Contact WHERE Email = :usr.Email];
             system.AssertEquals(usr.LastName,Con.LastName);
        }  
    }
    static testMethod void setUpDataForexistingcon()
    {
        try{
                Contact cont=testdatautils.getServiceProvider();
                cont.Email='testuserepractice@Appointed.com';
                cont.isCreatedFromUser__c = true;
                update cont;
                Profile prof = [SELECT Id FROM Profile WHERE Name =:Utils.serviceProviderProfile]; 
                system.debug('**prof**'+prof);
                User usr = new User(Alias = 'standt', Email='testuserepractice@Appointed.com', 
                    EmailEncodingKey='UTF-8', LastName='ePracticeTesting', LanguageLocaleKey='en_US', 
                    LocaleSidKey='en_US', ProfileId = prof.Id, 
                    TimeZoneSidKey='America/Los_Angeles', UserName='testuserepractice@appointed.com');
                insert usr;
                system.debug('*** USR ***'+usr); 
            }
        catch(exception e)
        {
            Boolean expectedExceptionThrown =  e.getMessage().contains('Service Provider') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }
}