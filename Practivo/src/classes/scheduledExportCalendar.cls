public class scheduledExportCalendar implements Schedulable {
   public void execute(SchedulableContext SC) 
   {
         try
         {
              List<Contact> lstSPs;
              if(Utils.getReadAccessCheck('Contact',new String []{'Id','Name','Salutation'}))
                  lstSPs=[SELECT id,name,Salutation FROM contact WHERE Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_ServiceProviderRecordType) LIMIT 2000]; 
              else
                    throw new Utils.ParsingException('No Read access to Contact or Fields in Contact.');
              SendNotificationEmails.sendscheduledExportCalendar(lstSPs);
         }
         catch(Exception e)
         {
             System.debug('scheduledExportCalendar-->'+e.getmessage());
         } 
   }
}