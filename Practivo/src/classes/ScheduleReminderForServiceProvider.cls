Public class ScheduleReminderForServiceProvider  implements Schedulable
{
    Public void execute(SchedulableContext SC) 
    {
        list<event> listtomorrowsevents = new list<event>();
        list<Messaging.SingleEmailMessage> ListSingleMails = new list<Messaging.SingleEmailMessage>();
        EmailTemplate EmailTempCustomerReminder;
        try
        {
            if(Utils.getReadAccessCheck('Event',new String []{'Id','IsRecurrence','StartDateTime','EndDateTime','WhoId','Event_Type__c'}))
                listtomorrowsevents=[SELECT Event_Type__c,Id,IsRecurrence,EndDateTime,WhoId FROM Event WHERE DAY_ONLY(EVENT.EndDateTime) =TOMORROW AND (Event_Type__c =:String.escapeSingleQuotes(Utils.EventType_Appointment) OR Event_Type__c =:String.escapeSingleQuotes(Utils.EventType_Partial)) AND Appointment_Type__r.Maximum_Participant__c>1 AND IsRecurrence=false];
            else
                throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
            if(Utils.getReadAccessCheck('EmailTemplate',new String []{'Id'}))    
                EmailTempCustomerReminder=[Select id from EmailTemplate where DeveloperName=:String.escapeSingleQuotes('Email_Template_Pract_Reminder')];
            else
                throw new Utils.ParsingException('No Read access to EmailTemplate or Fields in EmailTemplate.');
            for(EVENT ev:listtomorrowsevents)
            {
                Messaging.SingleEmailMessage MailSP = new Messaging.SingleEmailMessage();
                MailSP.setTargetObjectId(ev.whoid);
                MailSP.setWhatId(ev.id);     
                MailSP.setSenderDisplayName('Reminder Of Appointment');
                MailSP.setUseSignature(false);
                MailSP.setBccSender(false);
                MailSP.setSaveAsActivity(false);  
                MailSP.setTemplateId(EmailTempCustomerReminder.id);
                ListSingleMails.add(MailSP);
            }
        if(!ListSingleMails.isEmpty())
            {
                Messaging.SendEmailResult [] mailresult = Messaging.sendEmail(ListSingleMails);
            }
        }
        catch(exception e)
        {

        }
    }
}