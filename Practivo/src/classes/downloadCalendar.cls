/*
***************************************
 class name-downloadCalendar
 Class Description-Class for rest webservice which is used to get ical feed to service provider.
 *************************************
*/
@RestResource(urlMapping='/Appointed') 
global class downloadCalendar {
    
    /*
        Author : Koustubh Kulkarni.
        Description : This Httpget method is used for getting ics file attachment of service provider. 
                      When third party application hits this url along with fileID parameter in the link,
                      it will fetch particular record and returns response in calendar format.
    */
    @HttpGet
    global static void getCalendar()
    {
        String fileId = RestContext.request.params.get('fileId');
        fileId=String.escapeSingleQuotes(fileId);
        try{
        if(fileId!=null && !String.isBlank(fileId))
        {
            Attachment a=new Attachment();
            if(Utils.getReadAccessCheck('Attachment',new String []{'Id','name','body','description'}))
                a=[select id,name,body,description from Attachment where id=:fileId LIMIT 2000];    
            else
                throw new Utils.ParsingException('No Read access to Attachment or Fields in Attachment.');
            RestContext.response.addHeader('Content-Type','text/Calendar charset=utf-8');
            RestContext.response.addHeader('Access-Control-Allow-Origin', '*');
            RestContext.response.responseBody= a.body;
        }
        else
        {
            RestContext.response.addHeader('Content-Type','text/text');
            //RestContext.response.addHeader('Access-Control-Allow-Origin', '*');
            RestContext.response.responseBody= blob.valueof('Please Provide File Id');
        }
        }catch(Exception exp)
        {
            System.debug('--------------'+exp);
        }
    }
   
}