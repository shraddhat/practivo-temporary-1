/*
***************************************
 class name-BatchIcalFeedGeneration
 Class Description-Batch as well as scheduler class for running batch operation for updating ics attachment for all the service providers.
                    Scheduler will schedule half hour jobs to update the ics attachment associated with SP.
  @Author : Koustubh Kulkarni                  
 *************************************
*/
global class BatchIcalFeedGeneration implements Database.Batchable<sObject>,  Schedulable
{
    
    global Database.QueryLocator start(Database.BatchableContext BC) 
    {
      String query;
      try
        {
       EpracticeSettings__c schedulerSettings= EpracticeSettings__c.getInstance('ScheduleJobLastRunDateTime');
          if(schedulerSettings==null)
          {
            //Checks whether the job is running for fist time, if yes then inserts new custom setting
          schedulerSettings = new EpracticeSettings__c(Name = 'ScheduleJobLastRunDateTime', Value__c = 'First Time'); 
          if(Utils.getCreateAccessCheck(Utils.PackageNamespace+'EpracticeSettings__c', new String []{'Name','value__c'}))
              insert schedulerSettings;
          else
            throw new Utils.ParsingException('No create access to EpracticeSettings__c or Fields in EpracticeSettings__c.');    
          }
          if(schedulerSettings.Value__c=='First Time')
          {
           //query = 'SELECT Id, Name FROM Contact WHERE RecordType.developername = \'Service_Provider\' LIMIT 200';
           //if job is ruuning for the first time then it fetches all the events in the system
           if(Utils.getReadAccessCheck('Event',new String []{'Id','IsRecurrence','LastModifiedDate', 'WhoId', 'Event_Type__c'}))
             query='SELECT Event_Type__c,Id,IsRecurrence,LastModifiedDate,WhoId FROM Event WHERE (Event_Type__c = \'Appointment\' OR Event_Type__c = \'Partial\' OR Event_Type__c = \'Unavailable\') AND IsRecurrence=false LIMIT 2000';
           else
            throw new Utils.ParsingException('No Read access to Event or Fields in Event.'); 
          }
          else
          {
            //if job is not ruuning for the first time then it fetches all the events in the system from the date time when last job was ran 
              Datetime dt=Datetime.valueof(schedulerSettings.Value__c);
             if(Utils.getReadAccessCheck('Event',new String []{'Id','IsRecurrence','LastModifiedDate', 'WhoId','Event_Type__c'})) 
              query='SELECT Event_Type__c,Id,IsRecurrence,LastModifiedDate,WhoId FROM Event WHERE LastModifiedDate >=:dt AND (Event_Type__c = \'Appointment\' OR Event_Type__c = \'Partial\' OR Event_Type__c = \'Unavailable\') AND IsRecurrence=false LIMIT 2000';
             else
              throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
          }
        }
        catch(exception e)
        {
          system.debug('BatchIcalFeedGeneration-->'+e.getmessage());
        }
        return Database.getQueryLocator(query);  
    }
   
    global void execute(Database.BatchableContext BC, List<Event> scope) 
    {
        set<id> setpracids=new set<id>();
        if(!scope.isEmpty())
        {
            for(Event e:scope)
            {
                if(e.WhoId!=null)
                {
                    setpracids.add(e.WhoId);
                    //set will contain only those service providers who have events created after last job run
                }
            }
        }
      GenerateCalendarAttachment gcal= new GenerateCalendarAttachment(setpracids);   
    }   
    
    global void finish(Database.BatchableContext BC) 
    {
         EpracticeSettings__c schedulerSetting= EpracticeSettings__c.getInstance('ScheduleJobLastRunDateTime');
      DateTime myDateTime = DateTime.now();
      String formatted = myDateTime.format('yyyy-MM-dd HH:mm:ss'); //update custom setting for last run date time
      schedulerSetting.Value__c=formatted;
      if(Utils.getUpdateAccessCheck(Utils.PackageNamespace+'EpracticeSettings__c', new String []{'value__c'}))
        update schedulerSetting;
      else
        throw new Utils.ParsingException('No update access to EpracticeSettings__c or Fields in EpracticeSettings__c.');  
    }
    global void execute(SchedulableContext SC) 
   {
   BatchIcalFeedGeneration b=new BatchIcalFeedGeneration();
   database.executebatch(b);
    } 
}