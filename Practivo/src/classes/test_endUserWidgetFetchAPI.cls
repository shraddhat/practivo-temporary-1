@isTest
public with sharing class test_endUserWidgetFetchAPI {
    
  static testMethod void testgetCalendar()
  {
      
    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();
    req.requestURI = 'http://testprac-developer-edition.ap2.force.com/services/apexrest/endUserWidget/fetch';  

        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res; 
   endUserWidgetFetchAPI.getEndUserWidgetLeftPane();
      System.assertNotEquals(null, res.responseBody);
  }
    static testMethod void testEndUserWidgetWeeklyCalendarAndEvents() 
    {
        contact sp=TestDataUtils.getServiceProvider();   
         Category__c Cat=TestDataUtils.getcategory();
         Location__c loc=TestDataUtils.getlocation();
         Location_Category_Mapping__c Locat=TestDataUtils.getlocationmapping(loc,Cat,sp);
         AppointmentType__c atype=TestDataUtils.getappointment(cat);
        Resource_Type__c resTypass = TestDataUtils.getResourceTypeAssistant();
        Contact Assistant=TestDatautils.getAssistant(resTypass);
        Resource_Type_Appointment_Type_Mapping__c restypaptyp1=TestDatautils.getresaptypemapping(resTypass,atype);
         Location_Category_Mapping__c Locat1=TestDataUtils.getlocationmapping(loc,Cat,Assistant);
            Resource_Type__c restypehuman=TestDataUtils.getResourceType();
            Resource_Type__c restype=TestDataUtils.getResourceTypeNH();
         Resources__c resour=TestDataUtils.getresources(loc, restype);
         Resource_Type_Appointment_Type_Mapping__c ResApmap=TestDataUtils.getresaptypemapping(restype, atype);    
        TestDataUtils.insertWorkingTimeSlot(sp, restypehuman, Locat);
        TestDataUtils.geteventappointmentwithall(Sp,Locat,atype);
        Datetime Datetimenow=datetime.now();
        //do POST request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/endUserWidget/fetch';
        req.httpMethod = 'POST';
        req.addParameter('selectLocationfornewapp',loc.Id);
        req.addParameter('selectCategoryfornewapp',cat.id);
        req.addParameter('selectatfornewapp',atype.id);
        req.addParameter('selectPractitionerfornewapp',Sp.Id);
        req.addParameter('isClass','true');
        req.addParameter('appointmentDate',String.valueof(Datetimenow.day())+','+String.valueof(Datetimenow.month())+','+String.valueof(Datetimenow.year()));
        req.addParameter('Fetch_Method','Fetch_Daily_Available_TimeSlot');
        req.addParameter('isAnyPractitioner','false');
        RestContext.request = req;
        RestContext.response = res;

        endUserWidgetFetchAPI.getEndUserWidgetMonthlyCalendarAndEvents();
        System.assertNotEquals(null, res.responseBody);
    }
    static testMethod void testEndUserWidgetWeeklyCalendarAndEventsNotclass() 
    {
        contact sp=TestDataUtils.getServiceProvider(); 
         Category__c Cat=TestDataUtils.getcategory();
         Location__c loc=TestDataUtils.getlocation();
         Location_Category_Mapping__c Locat=TestDataUtils.getlocationmapping(loc,Cat,sp);
         AppointmentType__c atype=TestDataUtils.getappointment(cat);
        Resource_Type__c resTypass = TestDataUtils.getResourceTypeAssistant();
        Contact Assistant=TestDatautils.getAssistant(resTypass);
        Resource_Type_Appointment_Type_Mapping__c restypaptyp1=TestDatautils.getresaptypemapping(resTypass,atype);
         Location_Category_Mapping__c Locat1=TestDataUtils.getlocationmapping(loc,Cat,Assistant);
            Resource_Type__c restypehuman=TestDataUtils.getResourceType();
            Resource_Type__c restype=TestDataUtils.getResourceTypeNH();
         Resources__c resour=TestDataUtils.getresources(loc, restype);
         Resource_Type_Appointment_Type_Mapping__c ResApmap=TestDataUtils.getresaptypemapping(restype, atype);    
        TestDataUtils.insertWorkingTimeSlot(sp, restypehuman, Locat);
        TestDataUtils.geteventappointmentwithall(Sp,Locat,atype);
        Datetime Datetimenow=datetime.now();
        //do POST request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/endUserWidget/fetch';
        req.httpMethod = 'POST';
        req.addParameter('selectLocationfornewapp',loc.Id);
        req.addParameter('selectCategoryfornewapp',cat.id);
        req.addParameter('selectatfornewapp',atype.id);
        req.addParameter('selectPractitionerfornewapp','Any');
        req.addParameter('isClass','false');
        req.addParameter('appointmentDate',String.valueof(Datetimenow.day())+','+String.valueof(Datetimenow.month())+','+String.valueof(Datetimenow.year()));
        req.addParameter('Fetch_Method','Fetch_Daily_Available_TimeSlot');
        req.addParameter('isAnyPractitioner','true');
        RestContext.request = req;
        RestContext.response = res;

        endUserWidgetFetchAPI.getEndUserWidgetMonthlyCalendarAndEvents();
        System.assertNotEquals(null, res.responseBody);
    }
    static testMethod void testEndUserWidgetMonthlyCalendarAndEventsSpecificSp() 
    {
        contact sp=TestDataUtils.getServiceProvider(); 
         Category__c Cat=TestDataUtils.getcategory();
         Location__c loc=TestDataUtils.getlocation();
         Location_Category_Mapping__c Locat=TestDataUtils.getlocationmapping(loc,Cat,sp);
         AppointmentType__c atype=TestDataUtils.getappointment(cat);
        Resource_Type__c resTypass = TestDataUtils.getResourceTypeAssistant();
        Contact Assistant=TestDatautils.getAssistant(resTypass);
        Resource_Type_Appointment_Type_Mapping__c restypaptyp1=TestDatautils.getresaptypemapping(resTypass,atype);
         Location_Category_Mapping__c Locat1=TestDataUtils.getlocationmapping(loc,Cat,Assistant);
            Resource_Type__c restypehuman=TestDataUtils.getResourceType();
            Resource_Type__c restype=TestDataUtils.getResourceTypeNH();
         Resources__c resour=TestDataUtils.getresources(loc, restype);
         Resource_Type_Appointment_Type_Mapping__c ResApmap=TestDataUtils.getresaptypemapping(restype, atype); 
        TestDataUtils.insertWorkingTimeSlot(sp, restypehuman, Locat);
        TestDataUtils.geteventappointmentwithall(Sp,Locat,atype);
        Datetime Datetimenow=datetime.now();
        //do POST request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/endUserWidget/fetch';
        req.httpMethod = 'POST';
        req.addParameter('selectLocationfornewapp',loc.Id);
        req.addParameter('selectCategoryfornewapp',cat.id);
        req.addParameter('selectatfornewapp',atype.id);
        req.addParameter('selectPractitionerfornewapp',Sp.Id);
        req.addParameter('isClass','true');
        req.addParameter('appointmentDate',String.valueof(Datetimenow.day())+','+String.valueof(Datetimenow.month())+','+String.valueof(Datetimenow.year()));
        req.addParameter('Fetch_Method','Fetch_Monthly_Calendar');
        req.addParameter('isAnyPractitioner','false');
        RestContext.request = req;
        RestContext.response = res;

        endUserWidgetFetchAPI.getEndUserWidgetMonthlyCalendarAndEvents();
        System.assertEquals(null, res.responseBody);
    }
    static testMethod void testEndUserWidgetMonthlyCalendarAndEventsAny() 
    {
        contact sp=TestDataUtils.getServiceProvider();
         Category__c Cat=TestDataUtils.getcategory();
         Location__c loc=TestDataUtils.getlocation();
         Location_Category_Mapping__c Locat=TestDataUtils.getlocationmapping(loc,Cat,sp);
         AppointmentType__c atype=TestDataUtils.getappointment(cat);
        Resource_Type__c resTypass = TestDataUtils.getResourceTypeAssistant();
        Contact Assistant=TestDatautils.getAssistant(resTypass);
        Resource_Type_Appointment_Type_Mapping__c restypaptyp1=TestDatautils.getresaptypemapping(resTypass,atype);
         Location_Category_Mapping__c Locat1=TestDataUtils.getlocationmapping(loc,Cat,Assistant);
            Resource_Type__c restypehuman=TestDataUtils.getResourceType();
            Resource_Type__c restype=TestDataUtils.getResourceTypeNH();
         Resources__c resour=TestDataUtils.getresources(loc, restype);
         Resource_Type_Appointment_Type_Mapping__c ResApmap=TestDataUtils.getresaptypemapping(restype, atype); 
        TestDataUtils.insertWorkingTimeSlot(sp, restypehuman, Locat);
        TestDataUtils.geteventappointmentwithall(Sp,Locat,atype);
        Datetime Datetimenow=datetime.now();
        //do POST request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/endUserWidget/fetch';
        req.httpMethod = 'POST';
        req.addParameter('selectLocationfornewapp',loc.Id);
        req.addParameter('selectCategoryfornewapp',cat.id);
        req.addParameter('selectatfornewapp',atype.id);
        req.addParameter('selectPractitionerfornewapp','Any');
        req.addParameter('isClass','false');
        req.addParameter('appointmentDate',String.valueof(Datetimenow.day())+','+String.valueof(Datetimenow.month())+','+String.valueof(Datetimenow.year()));
        req.addParameter('Fetch_Method','Fetch_Monthly_Calendar');
        req.addParameter('isAnyPractitioner','true');
        RestContext.request = req;
        RestContext.response = res;

        endUserWidgetFetchAPI.getEndUserWidgetMonthlyCalendarAndEvents();
        System.assertNotEquals(null, res.responseBody);
    }
    static testMethod void testEndUserWidgetMonthlyCalendarAndEventsAnyClass() 
    {
        contact sp=TestDataUtils.getServiceProvider();  
         Category__c Cat=TestDataUtils.getcategory();
         Location__c loc=TestDataUtils.getlocation();
         Location_Category_Mapping__c Locat=TestDataUtils.getlocationmapping(loc,Cat,sp);
         AppointmentType__c atype=TestDataUtils.getappointment(cat);
        Resource_Type__c resTypass = TestDataUtils.getResourceTypeAssistant();
        Contact Assistant=TestDatautils.getAssistant(resTypass);
        Resource_Type_Appointment_Type_Mapping__c restypaptyp1=TestDatautils.getresaptypemapping(resTypass,atype);
         Location_Category_Mapping__c Locat1=TestDataUtils.getlocationmapping(loc,Cat,Assistant);
            Resource_Type__c restypehuman=TestDataUtils.getResourceType();
            Resource_Type__c restype=TestDataUtils.getResourceTypeNH();
         Resources__c resour=TestDataUtils.getresources(loc, restype);
         Resource_Type_Appointment_Type_Mapping__c ResApmap=TestDataUtils.getresaptypemapping(restype, atype); 
        TestDataUtils.insertWorkingTimeSlot(sp, restypehuman, Locat);
        TestDataUtils.geteventappointmentwithall(Sp,Locat,atype);
        Datetime Datetimenow=datetime.now();
        //do POST request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/endUserWidget/fetch';
        req.httpMethod = 'POST';
        req.addParameter('selectLocationfornewapp',loc.Id);
        req.addParameter('selectCategoryfornewapp',cat.id);
        req.addParameter('selectatfornewapp',atype.id);
        req.addParameter('selectPractitionerfornewapp','Any');
        req.addParameter('isClass','true');
        req.addParameter('appointmentDate',String.valueof(Datetimenow.day())+','+String.valueof(Datetimenow.month())+','+String.valueof(Datetimenow.year()));
        req.addParameter('Fetch_Method','Fetch_Monthly_Calendar');
        req.addParameter('isAnyPractitioner','true');
        RestContext.request = req;
        RestContext.response = res;

        endUserWidgetFetchAPI.getEndUserWidgetMonthlyCalendarAndEvents();
        System.assertNotEquals(null, res.responseBody);
    }
}