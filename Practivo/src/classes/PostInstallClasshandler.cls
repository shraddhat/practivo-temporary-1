/*
this is Handler class for post install script.
*/
global class PostInstallClasshandler 
{
    global static void insertCustomSettingvalues()
    {
        try
        {
        EpracticeSettings__c mainStarttime= EpracticeSettings__c.getInstance('Main Start Time');
        if (mainStarttime== null)
        {
            mainStarttime= new EpracticeSettings__c(Name = 'Main Start Time', Value__c = '07:00');
            if(Utils.getCreateAccessCheck(Utils.PackageNamespace+'EpracticeSettings__c', new String []{'Name','value__c'}))
                insert mainStarttime;
         }    
          EpracticeSettings__c mainendtime= EpracticeSettings__c.getInstance('Main End Time');
        if (mainendtime== null)
        {
            mainendtime= new EpracticeSettings__c(Name = 'Main End Time', Value__c = '19:00');
            if(Utils.getCreateAccessCheck(Utils.PackageNamespace+'EpracticeSettings__c', new String []{'Name','value__c'}))
                insert mainendtime;
         }       
            List<Configuration_Labels__c> ListLabelChanger=new List<Configuration_Labels__c>();
             Configuration_Labels__c LabelChangerSettingsCategory= Configuration_Labels__c.getInstance('Category');
           Configuration_Labels__c LabelChangerSettingsLocation= Configuration_Labels__c.getInstance('Location');
           Configuration_Labels__c LabelChangerSettingsServiceProvider= Configuration_Labels__c.getInstance('Service Provider');
           Configuration_Labels__c LabelChangerSettingsCustomer= Configuration_Labels__c.getInstance('Customer');
           Configuration_Labels__c LabelChangerSettingsAssistant= Configuration_Labels__c.getInstance('Assistant');
           Configuration_Labels__c LabelChangerSettingsCategoryPlural= Configuration_Labels__c.getInstance('Category(Plural)');
           Configuration_Labels__c LabelChangerSettingsLocationPlural= Configuration_Labels__c.getInstance('Location(Plural)');
           Configuration_Labels__c LabelChangerSettingsServiceProviderPlural= Configuration_Labels__c.getInstance('Service Provider(Plural)');
           Configuration_Labels__c LabelChangerSettingsCustomerPlural= Configuration_Labels__c.getInstance('Customer(Plural)');
           Configuration_Labels__c LabelChangerSettingsAssistantPlural= Configuration_Labels__c.getInstance('Assistant(Plural)');
           if(LabelChangerSettingsLocation==null)
            {
                LabelChangerSettingsLocation = new Configuration_Labels__c(Name = 'Location', Value__c = 'Location');
                ListLabelChanger.add(LabelChangerSettingsLocation);
                //insert LabelChangerSettingsLocation;
            }
        if(LabelChangerSettingsServiceProvider==null)
        {
            LabelChangerSettingsServiceProvider = new Configuration_Labels__c(Name = 'Service Provider', Value__c = 'Service Provider');
            ListLabelChanger.add(LabelChangerSettingsServiceProvider);
            //insert LabelChangerSettingsServiceProvider;   
        }
        if(LabelChangerSettingsCustomer==null)
        {
            LabelChangerSettingsCustomer = new Configuration_Labels__c(Name = 'Customer', Value__c = 'Customer');
            ListLabelChanger.add(LabelChangerSettingsCustomer);
            //insert LabelChangerSettingsCustomer;   
        }
        if(LabelChangerSettingsCategory==null)
        {
            LabelChangerSettingsCategory = new Configuration_Labels__c(Name = 'Category', Value__c = 'Category');
            ListLabelChanger.add(LabelChangerSettingsCategory);
            //insert LabelChangerSettingsCategory;     
        }
        if(LabelChangerSettingsAssistant==null)
        {
            LabelChangerSettingsAssistant = new Configuration_Labels__c(Name = 'Assistant', Value__c = 'Assistant');
            ListLabelChanger.add(LabelChangerSettingsAssistant);
            //insert LabelChangerSettingsAssistant;   
        }
        if(LabelChangerSettingsLocationPlural==null)
            {
                LabelChangerSettingsLocationPlural = new Configuration_Labels__c(Name = 'Location(Plural)', Value__c = 'Locations');
                ListLabelChanger.add(LabelChangerSettingsLocationPlural);
                //insert LabelChangerSettingsLocationPlural;    
            }
            if(LabelChangerSettingsServiceProviderPlural==null)
            {
                LabelChangerSettingsServiceProviderPlural = new Configuration_Labels__c(Name = 'Service Provider(Plural)', Value__c = 'Service Providers');
                ListLabelChanger.add(LabelChangerSettingsServiceProviderPlural);
                //insert LabelChangerSettingsServiceProviderPlural;     
            }
            if(LabelChangerSettingsCustomerPlural==null)
            {
                LabelChangerSettingsCustomerPlural = new Configuration_Labels__c(Name = 'Customer(Plural)', Value__c = 'Customers');
                ListLabelChanger.add(LabelChangerSettingsCustomerPlural);
                //insert LabelChangerSettingsCustomerPlural;    
            }
             if(LabelChangerSettingsCategoryPlural==null)
            {
                LabelChangerSettingsCategoryPlural = new Configuration_Labels__c(Name = 'Category(Plural)', Value__c = 'Categories');
                ListLabelChanger.add(LabelChangerSettingsCategoryPlural);
                //insert LabelChangerSettingsCategoryPlural;     
            }
            if(LabelChangerSettingsAssistantPlural==null)
            {
                LabelChangerSettingsAssistantPlural = new Configuration_Labels__c(Name = 'Assistant(Plural)', Value__c = 'Assistants');
                ListLabelChanger.add(LabelChangerSettingsAssistantPlural);
                //insert LabelChangerSettingsAssistantPlural;     
            }
            if(Utils.getCreateAccessCheck(Utils.PackageNamespace+'Configuration_Labels__c', new String []{'Name','value__c'}))
                insert ListLabelChanger;
            else   
                throw new Utils.ParsingException('No Create access to Configuration_Labels__c or Fields in Configuration_Labels__c.');
        } 
        catch(exception e)
        {
            system.debug('PostInstallClasshandler-->'+e.getmessage());
        }    
    }
}