/*
***************************************
 class name-ExportSPAppointmentsController
 Class Description-Controller for VF component Cmp_ExportCalendar and Cmp_ExportCalendarcancelledevents
 *************************************
*/
public with sharing class ExportSPAppointmentsController
{    
    
    public list<Appointmentwrapper> listAppointmentwrapper{get;set;}
    public list<Appointmentwrapper> listCancelledAppointmentwrapper{get;set;}      
    public String UserTimezone{get;set;}
    public static list<contact> listContacts=new list<contact>();
    contact reccontact;
    public String renderingcondition{get{return renderingcondition;}set;}
    public ID ContactID{get{return ContactID;}set;} //This parameter is set by vf component
    //public ID EvWhatID{get{return EvWhatID;}set;}
   // public ID getContactID(){  }
    //'00328000009yg5b'ContactID  
   
        
    public ExportSPAppointmentsController()
    {
        try
        {
            TimeZone tz = UserInfo.getTimeZone();
            UserTimezone=tz.getID();
            if(Utils.getReadAccessCheck('Contact',new String []{'Id','Name','Salutation'}))
                listContacts=[SELECT id,name,Salutation FROM contact WHERE (Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_CustomerRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_ServiceProviderRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_AssistantRecordType)) LIMIT 2000];
            else
                throw new Utils.ParsingException('No Read access to Contact or Fields in Contact.');
        }
        catch(Exception e)
        {
            System.debug('ExportSPAppointmentsController-->'+e.getStackTraceString()+e.getMessage());
        }            
    } 
    /*
     @MethodName : getAppointmentwrapperlist
     @Return Type :  list<Appointmentwrapper>  
     @Author : Koustubh Kulkarni.
     @Description : This method is used to fetch all appointments for particular service provider in given date range. This method used in Cmp_ExportCalendar component.      
   */
    public list<Appointmentwrapper> getAppointmentwrapperlist()
    {
         try
           {    
                listAppointmentwrapper=createappwrapperList('Appointment',false);
           }
           catch(exception ex)
           {
                System.debug('ExportSPAppointmentsController-->getAppointmentwrapperlist-->'+ex.getmessage()+ex.getstacktracestring());
           }
            
        return listAppointmentwrapper;
    }
    /*
     @MethodName : getCancelledAppointmentwrapperlist
     @Return Type :  list<Appointmentwrapper>  
     @Author : Koustubh Kulkarni.
     @Description : This method is used to fetch all cancelled appointments for particular service provider in given date range.This method used in Cmp_ExportCalendarcancelledevents component.       
   */
    public list<Appointmentwrapper> getCancelledAppointmentwrapperlist()
    {
     try{    
                listCancelledAppointmentwrapper=createappwrapperList('Cancelled',false);               
        }
        catch(exception ex)
        {
            System.debug('ExportSPAppointmentsController-->getCancelledAppointmentwrapperlist-->'+ex.getmessage()+ex.getstacktracestring());
        }
            
        return listCancelledAppointmentwrapper;
    }

    public list<Appointmentwrapper> getWeeklyAppointmentwrapperlist()
    {
         try
           {    
                listAppointmentwrapper=createappwrapperList('Appointment',true);
           }
           catch(exception ex)
           {
                System.debug('ExportSPAppointmentsController-->getAppointmentwrapperlist-->'+ex.getmessage()+ex.getstacktracestring());
           }
            
        return listAppointmentwrapper;
    }

     public list<Appointmentwrapper> getWeeklyCancelledAppointmentwrapperlist()
    {
     try{    
                listCancelledAppointmentwrapper=createappwrapperList('Cancelled',true);               
        }
        catch(exception ex)
        {
            System.debug('ExportSPAppointmentsController-->getCancelledAppointmentwrapperlist-->'+ex.getmessage()+ex.getstacktracestring());
        }
            
        return listCancelledAppointmentwrapper;
    }

    public list<Appointmentwrapper> createappwrapperList(String calledFrom,boolean isscheduler)
    {
       list<Appointmentwrapper> lstappwrap=new list<Appointmentwrapper>();
        Date dstart;
        Date dend;
        try
        {
            if(Utils.getReadAccessCheck('Contact',new String []{'Id','Start_Date_To_Generate_Calendar__c','End_Date_To_Generate_Calendar__c'}))
                    reccontact=[SELECT ID,Start_Date_To_Generate_Calendar__c,End_Date_To_Generate_Calendar__c FROM contact WHERE ID=:String.escapeSingleQuotes(ContactID) LIMIT 2000];
                else
                    throw new Utils.ParsingException('No Read access to Contact or Fields in Contact.');
                if(!isscheduler)
                {   
                    dstart =Date.valueof(reccontact.Start_Date_To_Generate_Calendar__c); 
                    dend =Date.valueof(reccontact.End_Date_To_Generate_Calendar__c);
                }
                else if(isscheduler)
                {
                    dstart=Date.today();
                    dend=dstart.addDays(7);
                }
                Datetime dtstart = datetime.newInstance(dstart.year(), dstart.month(),dstart.day(),00,00,01);
                Datetime dtend = datetime.newInstance(dend.year(), dend.month(),dend.day(),23,59,59);
                System.debug('THE ID OF THE PRAC'+ContactID );
                if(calledFrom=='Appointment')
                {
                    if(Utils.getReadAccessCheck('Event',new String []{'Id','whoid','whatid','Event_Type__c','StartDateTime','EndDateTime'}))
                    for(Event app: [SELECT ID,whoid,whatid,Location_Category_Mapping__r.Location__r.Name,Appointment_Type__r.name,Event_Type__c,Appointment_Type__r.Maximum_Participant__c,
                                     StartDateTime,EndDateTime,(select id,relationid,eventid,IsWhat FROM eventrelations WHERE relationid!=:ContactID AND IsWhat=false) FROM Event  
                                     WHERE  whoid=:String.escapeSingleQuotes(ContactID) and StartDateTime>=:dtstart  and EndDateTime<=:dtend and (Event_Type__c=:Utils.EventType_Appointment OR Event_Type__c=:Utils.EventType_Partial OR Event_Type__c=:Utils.EventType_Unavailable) and IsRecurrence=false LIMIT 2000])
                    {
                        lstappwrap.add(new Appointmentwrapper(app));
                    }  
                    else
                        throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
                }
                else if(calledFrom=='Cancelled')
                {
                    if(Utils.getReadAccessCheck('Event',new String []{'Id','whoid','whatid','Event_Type__c','StartDateTime','EndDateTime'}))    
                    for(Event app: [SELECT ID,whoid,Location_Category_Mapping__r.Location__r.Name,Appointment_Type__r.name,Event_Type__c,Appointment_Type__r.Maximum_Participant__c,
                                         StartDateTime,EndDateTime,(select id,relationid,eventid FROM eventrelations WHERE relationid!=:ContactID) from Event  
                                         WHERE  whoid=:String.escapeSingleQuotes(ContactID) and StartDateTime>=:dtstart  and EndDateTime<=:dtend and Event_Type__c='Cancelled' and IsRecurrence=false LIMIT 2000])
                    {
                        lstappwrap.add(new Appointmentwrapper(app));
                    }   
                    else
                        throw new Utils.ParsingException('No Read access to Event or Fields in Event.');
                }    
        }
        catch(Exception e)
        {
            System.debug('ExportSPAppointmentsController-->createappwrapperList-->'+e.getStackTraceString()+e.getMessage());
        }
        return lstappwrap;
    }
     public static string getformateddate(Datetime appdate) 
         {
            //This method converts given date in ical supported format which is like follow
            //2015-12-01T00:00:00
            //2015-12-10T00:00:00
             String s;
             String s1;
             String s2;
             String s3;
             String s4;
             String s5;
             String s6;
             String s7;
             String formattedstring;
            
             Date myDate = date.newinstance(appdate.year(), appdate.month(), appdate.day());
             s=string.valueof(myDate);
             s1=s.mid(0,4);
             s2=s.mid(5,2);
             s3=s.mid(8,2);
             s4=string.valueof(appdate);
             s5=s4.mid(11,2);
             s6=s4.mid(14,2);
             s7=s4.mid(17,2);
             formattedstring=s1+s2+s3+'T'+s5+s6+s7;
        
        return formattedstring;
         }
   public class Appointmentwrapper   
     {
         public Event Appointment{get;set;}
         public list<eventrelation> eventrels{get;set;}
         public list<string> listpatientnames{get;set;}
         public String FormattedStartDate{get;set;}
         public String FormattedEndDate{get;set;}
         public String Randomnumber{get;set;}
         public String practitionername{get;set;}
         public String patientname{get;set;}
         public String Description{get;set;}
         public String Summary{get;set;}
          
         public Appointmentwrapper(Event app)
         {
            Configuration_Labels__c LabelChangerSettingsCustomer= Configuration_Labels__c.getInstance('Customer');
             this.Appointment=app;
             this.practitionername=getname(app.whoid);
             this.eventrels=app.eventrelations;
             if(!app.eventrelations.isEmpty())
             {
             this.patientname=getname(app.eventrelations[0].relationid);
             Set<id> setrelids=new set<id>();
             listpatientnames=new list<string>();
             for(eventrelation ER:app.eventrelations)
                 {
                 setrelids.add(ER.relationid);
                 }
              //list<contact> lstpatients=[select id,name from contact where id in :setrelids];
             for(contact con:listContacts)
                 {
                    if(setrelids.contains(con.ID))
                    {
                        listpatientnames.add(con.name);
                    }
                 }
             }
             this.FormattedStartDate=getformateddate(app.StartDateTime);
             this.FormattedEndDate=getformateddate(app.EndDateTime);
             this.Randomnumber=String.valueOf(Crypto.getRandomLong());
             if(app.Appointment_Type__r.Maximum_Participant__c>1 && app.Event_Type__c!=Utils.EventType_Unavailable)
                {
                    this.Description = 'Hi '+getname(app.whoid)+',\\nHere are the '+String.valueOf(LabelChangerSettingsCustomer.value__c)+' names for your group appointment:\\n ';
                    this.Summary = app.Appointment_Type__r.name;
                }
            else if(app.Appointment_Type__r.Maximum_Participant__c <=1 && app.Event_Type__c!=Utils.EventType_Unavailable)
                {
                    this.Description = 'Hi '+getname(app.whoid)+',\\nYou have new appointment. Appointment details are:-\\n '+String.valueOf(LabelChangerSettingsCustomer.value__c)+' name-\\n ';
                    this.Summary =app.Appointment_Type__r.name+' with '+listpatientnames.get(0);
                }
            else if(app.Event_Type__c==Utils.EventType_Unavailable)
                {
                     this.Description = 'Hi '+getname(app.whoid)+',\\nThis is your unavailable timeslot.\\n ';
                     this.Summary = 'Unavailable timeslot';
                }
         }
         public string getname(id whoid)
        {
            string name;
            try
            {
                contact con=new contact();
                for(contact c:listContacts)
                {
                    if(c.ID==whoid)
                    {
                        con=c;
                    }
                }
                if(con.Salutation!=null || con.Salutation!='')
                    name=con.Salutation+con.Name;
                else
                    name=con.Name;    
            }
            catch(exception ex)
            {
                System.debug('ExportSPAppointmentsController-->getname-->'+ex.getmessage()+ex.getstacktracestring());
            }
            return name;
        }
        
     }
}