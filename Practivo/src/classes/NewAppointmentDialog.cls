public class NewAppointmentDialog
    {
        /*public List<SelectOption> newOldPatientOptions
        {
            get
            {
                if (newOldPatientOptions == null)
                {
                    newOldPatientOptions = new List<SelectOption>();
                    newOldPatientOptions.add(new SelectOption('false', 'Existign Patient'));
                    newOldPatientOptions.add(new SelectOption('true', 'Existign Patient'));
                }
                return newOldPatientOptions;
            }
            set;
        }
        Event appointment;
        public String eventType {get;set;}
        public String title {get;set;}
        public Contact newContact {get;set;}
        public Appointment__c newAppointment {get; set;}
        public Id selectedPractitionerId {get;set;}
        public Id selectedLocation {get;set;}
        public Id selectedCategory {get;set;}
        public Id selectedAppointmentType {get;set;}
        public String birthdayInput {get;set;}
        public String dateInput {get;set;}
        public String startTimeInput {get;set;}
        public String endTimeInput {get;set;}
        public Boolean isSavedSuccessfully {get;set;}
        public Boolean isNewCustomer {get;set;}
        public Boolean skipTimeValidation {get;set;}
        
        public Boolean isShowPatient {get;set;}
        
        public List<SelectOption> recurringOptions {get;set;}
        public String selectedRecurring {get;set;}
        public Integer recurringEvery {get;set;}
        public Integer recurringCount {get;set;}
        public List<SelectOption> typesList {get;set;}
        
        public NewAppointmentDialog()
        {}
  }
        /*public NewAppointmentDialog(Id theId)
        {
            this();
            newAppointment = [SELECT Id, StartDate__c, EndDate__c, Practitioner__c, Patient__c, AppointmentType__c, Note__c, 
                    AppointmentType__r.RecordTypeId, AppointmentType__r.RecordType.DeveloperName
                    FROM Appointment__c WHERE Id = :theId FOR VIEW];
            eventType = newAppointment.AppointmentType__c != null && newAppointment.AppointmentType__r.RecordTypeId != null 
                    ? newAppointment.AppointmentType__r.RecordType.DeveloperName : eventType;
            //if(!isExecuated)
                init();
        }
        
        public NewAppointmentDialog()
        {
            this(ScheduleWizardController.APPOINTMENT_TYPE_PATIENT);
            appointment = new Event();
        }
        
        public NewAppointmentDialog(String theEventType)
        {
            newContact = new Contact();
            system.debug('initialising conatc');
            newAppointment = new Appointment__c();
            isSavedSuccessfully = false;
            isNewCustomer = false;
            skipTimeValidation = false;
            eventType = theEventType; //== null ? ScheduleWizardController.APPOINTMENT_TYPE_PATIENT : theEventType;
            init();
        }
        
        public void init()
        {
            /*typesList = new List<SelectOption>();
            typesList.add(new SelectOption('', '--Select Event Type--'));
            for (AppointmentType__c aType: ScheduleWizardController.typesMap.values())
            {
                if (aType.RecordTypeId != null && aType.RecordType.DeveloperName == eventType)
                {
                    typesList.add(new SelectOption(aType.Id, aType.Name));
                }
            }
            dateInput = newAppointment.StartDate__c == null ? null : newAppointment.StartDate__c.format(ScheduleWizardController.dateFormatStatic);
            startTimeInput = newAppointment.StartDate__c == null ? null : newAppointment.StartDate__c.format('HH:mm');
            endTimeInput = newAppointment.EndDate__c == null ? null : newAppointment.EndDate__c.format('HH:mm');
            
            recurringOptions = new List<SelectOption>();
            recurringOptions.add(new SelectOption('', 'Never'));
            for (String aType: ScheduleWizardController.kRECURRING_TYPES)
            {
                recurringOptions.add(new SelectOption(aType, aType));
            }
            recurringEvery = 1;
            recurringCount = 1;
            isSavedSuccessfully = false;
            title = eventType == ScheduleWizardController.APPOINTMENT_TYPE_PATIENT
                    ? (newAppointment.Id == null ? ScheduleWizardController.TITLE_APPOINTMENT_NEW : ScheduleWizardController.TITLE_APPOINTMENT_EDIT)
                    : (newAppointment.Id == null ? ScheduleWizardController.TITLE_APPOINTMENT_NEW_UNAVAILABILITY : ScheduleWizardController.TITLE_APPOINTMENT_EDIT_UNAVAILABILITY);
            isShowPatient = eventType != ScheduleWizardController.APPOINTMENT_TYPE_UNAVAILABLE;
            if (eventType == ScheduleWizardController.APPOINTMENT_TYPE_UNAVAILABLE)
            {
                skipTimeValidation = true;
            }
            //isExecuated = true;
        }
        
        
        
        private void updateTime()
        {
            selectedRecurring = ApexPages.currentPage().getParameters().get('recurringType');
            try
            {
                newContact.Birthdate = birthdayInput == null || birthdayInput == '' ? null : Date.parse(birthdayInput);
            } catch (Exception theException)
            {
                newContact.Birthdate = null;
            }
            newAppointment.StartDate__c = null;
            newAppointment.EndDate__c = null;
            try
            {
                Date aDate = Date.parse(dateInput);
                if (aDate  != null)
                {
                    newAppointment.StartDate__c = Datetime.newInstance(aDate, Utils.parseTime(startTimeInput));
                    newAppointment.EndDate__c = Datetime.newInstance(aDate, Utils.parseTime(endTimeInput));
                }
            } catch (Exception theException)
            {
               system.debug('*** ERROR in Update Time--'+theException);
            }
        }
        
        public void doChangeType()
        {
            /*updateTime();
            if (newAppointment.StartDate__c != null && newAppointment.AppointmentType__c != null)
            {
                Integer aDuration = 60;
                AppointmentType__c aType = ScheduleWizardController.typesMap.get(newAppointment.AppointmentType__c);
                aDuration = aType == null || aType.DefaultDuration__c == null ? aDuration : Integer.valueOf(aType.DefaultDuration__c);
                newAppointment.EndDate__c = newAppointment.StartDate__c.addMinutes(aDuration);
                endTimeInput = newAppointment.EndDate__c.format('HH:mm'); 
            }
        }
              
        public void saveAppointment()
        {
            appointment.OwnerId=UserInfo.getUserId();
            appointment.Subject='Appointment For ';
        }
        public pageReference doSaveAppointment()
        {
            system.debug('selectedLocation--'+selectedLocation);
            system.debug('selectedCategory--'+selectedCategory);
            system.debug('selectedPractitionerId--'+selectedPractitionerId);
            system.debug('selectedAppointmentType--'+selectedAppointmentType);
            if (newAppointment.Id == null)
            {
                Location_Category_Mapping__c locationCategory = [SELECT id FROM Location_Category_Mapping__c WHERE Location__c =:selectedLocation AND  Category__c = :selectedCategory AND Contact__C = :selectedPractitionerId LIMIT 1];
                if(locationCategory!=null)
                    newAppointment.Location_Category_Mapping__c = locationCategory.id;
                newAppointment.Practitioner__c = selectedPractitionerId;
                newAppointment.AppointmentType__c = selectedAppointmentType;
                String aNewExParameter = ApexPages.currentPage().getParameters().get('patientType');
                isNewCustomer = aNewExParameter != null && aNewExParameter.equalsIgnoreCase('newPatient');
                system.debug('in id null if');
            }
            updateTime();
            //if (!validateInput())
            {
              //  return;
            }
            try
            {
                if(isNewCustomer)
                {
                    newContact.RecordTypeId ='01228000000dZrL'; //patient record type Need to change this 
                    insert newContact;
                    newAppointment.Patient__c = newContact.Id;
                }
                else
                {
                    system.debug('patient selected=='+newAppointment.Patient__c);
                }                
               
                upsert newAppointment;
                system.debug('new App inserted with id'+newAppointment.id);
                isSavedSuccessfully = true;
                if (selectedRecurring != null && selectedRecurring != '' && recurringEvery > 0 && recurringCount > 0)
                {
                    newAppointment.GroupId__c = newAppointment.Id;
                    update newAppointment;
                    createRecurringEvents();
                }
              
            } catch (Exception theException)
            {
                ApexPages.addMessages(theException);
                System.debug('*** Error:' + theException);
            }
           
            return null;
        }
        
       private List<Appointment__c> createRecurringEvents()
        {
            /*Appointment__c aNewAppointment = newAppointment;
            List<Appointment__c> anAppointmentList = new List<Appointment__c>();
            for (Integer i = 0; i < recurringCount; i++)
            {
                aNewAppointment = aNewAppointment.clone();
                if (selectedRecurring == ScheduleWizardController.kREPEAT_DAILY)
                {
                    aNewAppointment.StartDate__c = aNewAppointment.StartDate__c.addDays(recurringEvery);
                    aNewAppointment.EndDate__c = aNewAppointment.EndDate__c.addDays(recurringEvery); 
                } else if (selectedRecurring == ScheduleWizardController.kREPEAT_WEEKLY)
                {
                    aNewAppointment.StartDate__c = aNewAppointment.StartDate__c.addDays(recurringEvery * 7);
                    aNewAppointment.EndDate__c = aNewAppointment.EndDate__c.addDays(recurringEvery * 7);
                } else if (selectedRecurring == ScheduleWizardController.kREPEAT_MONTHLY)
                {
                    aNewAppointment.StartDate__c = aNewAppointment.StartDate__c.addMonths(recurringEvery);
                    aNewAppointment.EndDate__c = aNewAppointment.EndDate__c.addMonths(recurringEvery);
                }
                anAppointmentList.add(aNewAppointment);
            }
            insert anAppointmentList;
            return anAppointmentList;
        }
        
        public void doCreateFollowUp()
        {
            /*newAppointment = newAppointment.clone();
            newAppointment.StartDate__c = null;
            newAppointment.EndDate__c = null;
            init();
            title = ScheduleWizardController.TITLE_APPOINTMENT_FOLLOW;
        }
        
        public void doDeleteAppointment()
        {
            if (newAppointment.Id != null)
            {
                try
                {
                    delete newAppointment;
                } catch (Exception theException)
                {
                    ApexPages.addMessages(theException);
                    System.debug('*** Error:' + theException);
                    isSavedSuccessfully = false;
                    return;
                }
                isSavedSuccessfully = true;
            }
        }
        public Boolean validateInput()
        {
            /*Boolean aResult = true;
            if (newAppointment.StartDate__c == null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a correct start time'));
                aResult = false;
            }
            if (newAppointment.EndDate__c == null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a correct end time'));
                aResult = false;
            }
            if (newAppointment.StartDate__c != null && newAppointment.EndDate__c != null
                     && newAppointment.StartDate__c >= newAppointment.EndDate__c)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'End time should be less then start time'));
                aResult = false;
            }
            if (isNewCustomer)
            {
                if (newContact.Birthdate == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a correct Birthdate'));
                    aResult = false;
                }
                if (newContact.LastName == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a correct Last Name'));
                    aResult = false;
                }
            } else
            {
                if (eventType != ScheduleWizardController.APPOINTMENT_TYPE_UNAVAILABLE && newAppointment.Patient__c == null)
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a Patient'));
                    aResult = false;
                }
            }
            if (newAppointment.Practitioner__c == null)
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a Practitioner'));
                aResult = false;
            }
            if (newAppointment.AppointmentType__c == null)
            {
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter an Appointment Type'));
                //aResult = false;
            }
            if (recurringCount != null && (recurringCount < 0 || recurringCount > 100))
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a correct Recurring Count'));
                aResult = false;
            }
            if (aResult == false)
            {
                return aResult;
            }
            if (!skipTimeValidation && ScheduleWizardController.isSlotBusy(newAppointment))
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The selected time slot is busy.'));
                aResult = false;
            }
            /*if (!skipTimeValidation && !isSlotInWorkignTime(newAppointment))
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The selected time is not working for selected practitioner'));
                aResult = false;
            }
            //return aResult;
            return false;
        }*/
   }