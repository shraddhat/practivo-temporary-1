@istest
public class Test_AddtoWaitListController 
{
   
static testmethod void testdata()
    {
            Contact con =  Testdatautils.getCustomer();
            con.Email = 'Test@Setup.com';
           // con.RecordType.Developername='Customer';
            Update con;
        
            Contact con1 =  Testdatautils.getCustomer();
            con1.Email = 'Test@Setupdata.com';
            Update con1;
        
            Contact con2 =  Testdatautils.getCustomer();
            con2.Email = 'Sample@testdata.com';
            Update con2;
        
            Contact con3 =  new Contact();
            con3.FirstName = 'Test';
            con3.LastName = 'data';
            con3.Email = 'setup@Testdata.com';
            con3.phone='311313131';
            con3.Birthdate=Date.valueOf('2015-01-01');
        
           set <id> setconid= new set<id>();
            set <id> setconid2= new set<id>();
                setconid.add(con.id);
                setconid.add(con1.id);
                setconid2.add(con1.id);
        
        
        Location__c loc=new Location__c();
        loc.name='pune';
        loc.Address__c='Test 20,test road';
        insert loc;
        
        Category__c cat=new Category__c();
        cat.Name='physio';
        insert cat;
        
        AppointmentType__c apt= new AppointmentType__c();
        apt.Name='app1';
        apt.Category__c=cat.id;
        apt.Price__c=100;
        apt.Maximum_Participant__c=1;
        insert apt;
        
        Location_Category_Mapping__c locmap=new Location_Category_Mapping__c();
        locmap.Location__c=loc.id;
        locmap.Category__c=cat.id;
        locmap.Contact__c=con.id;
        insert locmap;
         
        Date StartDate1 = Date.Today();
        Time tym = Time.newInstance(9, 0, 0, 0);
        Time tym1 = Time.newInstance(11, 0, 0, 0);
        DateTime StartDateTime1 =  DateTime.newInstance(StartDate1, tym);
        DateTime endDateTime1 =  DateTime.newInstance(StartDate1, tym1);
        
        /* Event Eventdetails1=new event();
            Eventdetails1.subject='sample';
            Eventdetails1.Event_Type__c='Appointment';
            Eventdetails1.WhoId=null;
            Eventdetails1.IsRecurrence=false;
            Eventdetails1.StartDateTime=Datetimenow;
            Eventdetails1.EndDateTime=EndDt;
            insert Eventdetails1;*/
            //AddtoWaitListController.AlsoAddToWaitlist(Eventdetails1);
         
        String Savrdatajson;
        list<ScheduleWizardController.saveHelperClass> savHelpLst=new list<ScheduleWizardController.saveHelperClass>();
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappemail',''));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('extistingPatientEmail','newCustomer@test.com'));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappfname','New Customer'));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newapplname','Last Name'));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappPhone','54268232056'));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappBirthDate',''));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectLocationfornewapp',loc.Id));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectCategoryfornewapp',cat.Id));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectatfornewapp',apt.Id));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectPractitionerfornewapp',con.id));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappComments',''));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectPractitionerName','Test'));
        Savrdatajson=JSON.serialize(savHelpLst);
        map<string,string> addPatientData = new map<String,string>();
            list<Utils.jsonHelperClass> lst = new list<Utils.jsonHelperClass>();
            lst = (list<Utils.jsonHelperClass>)JSON.deserialize(Savrdatajson, list<Utils.jsonHelperClass>.Class); 
            for(Utils.jsonHelperClass addPatientObj : lst)
            {
                addPatientData.put(addPatientObj.name,addPatientObj.value);
            }
        test.startTest();
        AddtoWaitListController awc= new AddtoWaitListController();
        AddtoWaitListController.add(addPatientData);
        test.stopTest();
        // Query the database for the newly inserted records.
    List<Event> insertedevents = [SELECT Id,whoid FROM event];
        
    // Assert that the Description fields contains the proper value now.
    for(event a : insertedevents)
    {
      System.assertEquals(con.id, a.whoid);
    }
    }
    Static testmethod void testalsoaddtowait()
    {
        Profile prof = [SELECT Id FROM Profile WHERE Name ='System Administrator'];
		User usr = [SELECT ID FROM USER WHERE ProfileId=:prof.Id Limit 1];
  		system.debug(usr);
        system.runAs(usr)
        {
            Contact con =  Testdatautils.getCustomer();
            con.Email = 'Test@Setup.com';
            // con.RecordType.Developername='Customer';
            Update con;
            
            Contact con2 =  Testdatautils.getCustomer();
            con2.Email = 'Sample@testdata.com';
            Update con2;
            
            Datetime Datetimenow=datetime.now();
            Datetime EndDt=Datetimenow.addhours(5);
            
            Event Eventdetails=new event();
            Eventdetails.subject='sample';
            Eventdetails.Event_Type__c='Appointment';
            Eventdetails.whoid=con.Id;
            Eventdetails.IsRecurrence=false;
            Eventdetails.StartDateTime=Datetimenow;
            Eventdetails.EndDateTime=EndDt;
            insert Eventdetails;
            EventRelation evrel=new EventRelation();
            evrel.EventId=Eventdetails.id;
            evrel.RelationId=con2.id;
            evrel.isparent=true;
            insert evrel;
            test.startTest();
            AddtoWaitListController.alsoaddtowait(Eventdetails.id, con2.id);
            test.stopTest();
            
            List<Event> insertedevents = [SELECT Id,whoid,Event_Type__c FROM event WHERE Event_Type__c='Waiting'];
            System.assertEquals(1,insertedevents.size());
        }
    }
    static testmethod void testdataNegative()
    {
        Contact con =  new Contact();
            con.FirstName='Anil';
            con.LastName = 'Dutt';
            con.Email = 'anil@swiftsestup.com';
           //con.RecordType.Developername='Service_Provider';
            insert con;
        
        Contact con1 = TestDataUtils.getCustomer();
        
         Location__c loc=new Location__c();
        loc.name='pune';
        loc.Address__c='Test 20,test road';
        insert loc;
        
        Category__c cat=new Category__c();
        cat.Name='physio';
        insert cat;
        
        AppointmentType__c apt= new AppointmentType__c();
        apt.Name='app1';
        apt.Category__c=cat.id;
        apt.Price__c=100;
        apt.Maximum_Participant__c=1;
        insert apt;
        
        String Savrdatajson;
        list<ScheduleWizardController.saveHelperClass> savHelpLst=new list<ScheduleWizardController.saveHelperClass>();
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappemail','test@test.com'));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappfname','New Customer'));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newapplname','Last Name'));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappPhone','54268232056'));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappBirthDate',''));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectLocationfornewapp',loc.Id));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectCategoryfornewapp',cat.Id));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectatfornewapp',apt.Id));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectPractitionerfornewapp','Any'));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('newappComments',''));
        savHelpLst.add(new ScheduleWizardController.saveHelperClass('selectPractitionerName','Any'));
        Savrdatajson=JSON.serialize(savHelpLst);
        map<string,string> addPatientData = new map<String,string>();
            list<Utils.jsonHelperClass> lst = new list<Utils.jsonHelperClass>();
            lst = (list<Utils.jsonHelperClass>)JSON.deserialize(Savrdatajson, list<Utils.jsonHelperClass>.Class); 
            for(Utils.jsonHelperClass addPatientObj : lst)
            {
                addPatientData.put(addPatientObj.name,addPatientObj.value);
            }
        test.startTest();
        AddtoWaitListController awc= new AddtoWaitListController();
        AddtoWaitListController.add(addPatientData);
        test.stopTest();
         List<Event> insertedevents = [SELECT Id,whoid,Event_Type__c FROM event];
        System.assertEquals(1,insertedevents.size());
    }
}