trigger createServiceProvider on User (after insert) 
{
 
    try
    {
        RecordType RecordTyp;
        Profile profileUser;
        list<User> userLst = Trigger.New;
        list<PermissionSet> PermissionSetLSt = [SELECT Id,Name FROM PermissionSet LIMIT 5000];
        list<PermissionSetAssignment> lst =  [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId IN :Trigger.New];
        //[SELECT ID,Name FROM PermissionSet WHERE ID IN (SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId IN :Trigger.New)];
        list<Contact> exsitingServiceProviderLst;
        if(Utils.getReadAccessCheck('Recordtype',new String []{'Id'}))
            RecordTyp= [SELECT Id from RecordType where SobjectType='Contact' and DeveloperName =:Utils.contact_ServiceProviderRecordType Limit 1];
        else
            throw new Utils.ParsingException('No read access to Recordtype.');
        list<User> newUserList = Trigger.New;
        if(Utils.getReadAccessCheck('Profile',new String []{'Id','Name'}))
            profileUser= [SELECT Id,Name FROM Profile WHERE Name=:Utils.serviceProviderProfile LIMIT 1]; 
        else
            throw new Utils.ParsingException('No Read access to Profile or Fields in Profile.');   
        //ProfileId 
        if(Utils.getReadAccessCheck('contact',new String []{'Id','Name','FirstName','LastName','Phone','Email','RecordTypeId'}))
            exsitingServiceProviderLst = [SELECT Id,Name,FirstName,LastName,Email,RecordTypeId FROM Contact WHERE RecordTypeId = :RecordTyp.id AND isCreatedFromUser__c = true LIMIT 2000];
        else
            throw new Utils.ParsingException('No Read access to contact or Fields in contact.');
        boolean contactCreated = false;
        list<Contact> contactsLst = new list<Contact>();    
            for(User usr : newUserList)
            {
                if(profileUser!=null && usr.ProfileId == profileUser.Id)
                {
                    if(!exsitingServiceProviderLst.isEmpty())
                    {
                        for(Contact Con : exsitingServiceProviderLst)
                        {
                            if(Con.Email == usr.Email)
                            {
                                usr.addError('Service Provider :\''+ Con.Name +'\' has used this email id. Please use unique email Id');
                                contactCreated = true;
                                system.debug('User Trigger :: createServiceProvider :: Service Provider Already Created');
                            }
                        }
                    }
                    if(!contactCreated)
                    {   
                        Contact con = new Contact();
                        con.PractitionerColor__c = usr.Color__c;
                        con.FirstName = usr.FirstName;
                        con.LastName = usr.LastName;  
                        con.Phone = usr.Phone;  
                        con.Email = usr.Email;
                        con.MobilePhone = usr.MobilePhone;
                        con.RecordTypeId = RecordTyp.id;     
                        con.isCreatedFromUser__c = true;
      					system.debug('User Trigger :: createServiceProvider :: Adding New Service Provider');
                        contactsLst.add(con);
                    }
                }           
            }        
            upsert contactsLst; 
    }
    catch(Exception E)
    {
       system.debug('***Exception : createServiceProvider : '+E);
    }
}